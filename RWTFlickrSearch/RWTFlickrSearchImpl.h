//
//  RWTFlickrSearchImpl.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWTFlickrSearch.h"

@interface RWTFlickrSearchImpl : NSObject <RWTFlickrSearch>

@end
