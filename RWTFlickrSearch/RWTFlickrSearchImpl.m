//
//  RWTFlickrSearchImpl.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickrSearchImpl.h"
#import "RWTFlickrSearchResults.h"
#import "RWTFlickrPhoto.h"
#import <objectiveflickr/ObjectiveFlickr.h>
#import <LinqToObjectiveC/NSArray+LinqExtensions.h>
#import "RWTFlickrPhotoMetaData.h"
#import <ReactiveCocoa/RACEXTScope.h>
#import "RWTFlickrPhotoDetails.h"

@interface RWTFlickrSearchImpl () <OFFlickrAPIRequestDelegate>

@property (strong, nonatomic) NSMutableSet *requests;
@property (strong, nonatomic) OFFlickrAPIContext *flickrContext;

@end

@implementation RWTFlickrSearchImpl

- (instancetype)init {
    self = [super init];
    if (self) {
        NSString *OFSampleAppAPIKey = @"a183d5a9ad20a9621ef8b800dc4483b7";
        NSString *OFSampleAppAPISharedSecret = @"3d5742cdbf4229d4";
        _flickrContext = [[OFFlickrAPIContext alloc] initWithAPIKey:OFSampleAppAPIKey
                                                       sharedSecret:OFSampleAppAPISharedSecret];
        _requests = [NSMutableSet new];
    }
    
    return  self;
}

- (RACSignal *)flickrSearchSignal:(NSString *)searchString {
    return [self signalFromAPIMethod:@"flickr.photos.search"
                           arguments:@{@"text": searchString,
                                       @"sort": @"interestingness-desc"}
                           transform:^id(NSDictionary *response) {
                               
                               RWTFlickrSearchResults *results = [RWTFlickrSearchResults new];
                               results.searchString = searchString;
                               results.totalResults = [[response valueForKeyPath:@"photos.total"] integerValue];
                               
                               NSArray *photos = [response valueForKeyPath:@"photos.photo"];
                               results.photos = [photos linq_select:^id(NSDictionary *jsonPhoto) {
                                   RWTFlickrPhoto *photo = [RWTFlickrPhoto new];
                                   photo.title = [jsonPhoto objectForKey:@"title"];
                                   photo.identifier = [jsonPhoto objectForKey:@"id"];
                                   photo.url = [self.flickrContext photoSourceURLFromDictionary:jsonPhoto
                                                                                           size:OFFlickrSmallSize];
                                   return photo;
                               }];
                               
                               return results;
                           }];
}

- (RACSignal *)signalFromAPIMethod:(NSString *)method
                         arguments:(NSDictionary *)args
                         transform:(id (^)(NSDictionary *response))block {
    
    // 1. Create a signal for this request
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        // 2. Create a Flick request object
        OFFlickrAPIRequest *flickrRequest = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.flickrContext];
        flickrRequest.delegate = self;
        [self.requests addObject:flickrRequest];
        
        // 3. Create a signal from the delegate method
        RACSignal *successSignal = [self rac_signalForSelector:@selector(flickrAPIRequest:didCompleteWithResponse:)
                                                  fromProtocol:@protocol(OFFlickrAPIRequestDelegate)];
        
        // 4. Handle the response
        @weakify(flickrRequest)
        [[[[successSignal
            filter:^BOOL(RACTuple *tuple) {
                @strongify(flickrRequest)
                return tuple.first == flickrRequest;
            }]
           map:^id(RACTuple *tuple) {
               return tuple.second;
           }]
          map:block]
         subscribeNext:^(id x) {
             [subscriber sendNext:x];
             [subscriber sendCompleted];
         }];
        
        // 5. Make the request
        [flickrRequest callAPIMethodWithGET:method
                                  arguments:args];
        
        // 6. When we are done, remove the reference to this request
        return [RACDisposable disposableWithBlock:^{
            [self.requests removeObject:flickrRequest];
        }];
    }];
}

- (RACSignal *)flickrImageMetadata:(NSString *)imageidentifier {
    RACSignal *commentsSignal = [self signalFromAPIMethod:@"flickr.photos.getInfo"
                                                arguments:@{@"photo_id":imageidentifier}
                                                transform:^id(NSDictionary *response) {
                                                    return @([[response valueForKeyPath:@"photo.comments._text"] integerValue]);
                                                }];
    RACSignal *favoritesSignal = [self signalFromAPIMethod:@"flickr.photos.getFavorites"
                                                 arguments:@{@"photo_id":imageidentifier}
                                                 transform:^id(NSDictionary *response) {
                                                     NSNumber *favorites = [response valueForKeyPath:@"photo.total"];
                                                     
                                                     return favorites;
                                                 }];
    
    return [RACSignal combineLatest:@[commentsSignal, favoritesSignal] reduce:^id(NSNumber *comments, NSNumber *favorites){
        return [[RWTFlickrPhotoMetaData alloc] initWithFavorites:[favorites integerValue]
                                                        comments:[comments integerValue]];
    }];
}

- (RACSignal *)flickrImageDetails:(NSString *)imageidentifier {
    RACSignal *commentsListSignal = [self signalFromAPIMethod:@"flickr.photos.comments.getList"
                                                    arguments:@{@"photo_id":imageidentifier}
                                                    transform:^id(NSDictionary *response) {
                                                        return response;
                                                    }];
    RACSignal *infoSignal = [self signalFromAPIMethod:@"flickr.photos.getInfo"
                                            arguments:@{@"photo_id":imageidentifier}
                                            transform:^id(NSDictionary *response) {
                                                return response;
                                            }];
    return [RACSignal combineLatest:@[infoSignal, commentsListSignal]
                             reduce:^id(NSDictionary *infoResponse, NSDictionary *commentsListResponse) {
                                 NSString *description = [infoResponse valueForKeyPath:@"photo.description._text"];
                                 NSDate *created = [infoResponse valueForKeyPath:@"photo.dates.taken"];
                                 NSArray *comments = [commentsListResponse valueForKeyPath:@"comments.comment._text"];
                                 NSArray *tags = [infoResponse valueForKeyPath:@"photo.tags.tag._text"];
                                 RWTFlickrPhotoDetails *photoDetails = [[RWTFlickrPhotoDetails alloc]
                                                                        initWithDescription:description
                                                                        created:created
                                                                        comments:comments
                                                                        tags:tags];
                                 
                                 return photoDetails;
                             }];
}

@end
