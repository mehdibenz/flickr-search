//
//  RWTFlickrSearch.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
@import Foundation;

@protocol RWTFlickrSearch <NSObject>

- (RACSignal *)flickrSearchSignal:(NSString *)searchString;
- (RACSignal *)flickrImageMetadata:(NSString *)imageidentifier;
- (RACSignal *)flickrImageDetails:(NSString *)imageidentifier;

@end
