//
//  RWTViewModelServices.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//
@import Foundation;
#import "RWTFlickrSearch.h"

@protocol RWTViewModelServices <NSObject>

- (id<RWTFlickrSearch>)getFlickrSearchService;
- (void)pushViewModel:(id)viewModel;

@end
