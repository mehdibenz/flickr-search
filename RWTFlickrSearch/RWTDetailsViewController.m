//
//  RWTDetailsViewController.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "RWTDetailsViewController.h"
#import "CETableViewBindingHelper.h"
#import "RWTCommentTableViewCell.h"
#import "ReactiveCocoa/RACEXTScope.h"

@interface RWTDetailsViewController ()

@property (nonatomic, strong) RWTDetailsViewModel *viewModel;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;

@end

@implementation RWTDetailsViewController

- (instancetype)initWithViewModel:(RWTDetailsViewModel *)viewModel {
    self = [super init];
    if (self) {
        _viewModel = viewModel;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupViews];
    [self bindViewModel];
}

#pragma mark - View Model Binding

- (void)bindViewModel {
    [self.imageView sd_setImageWithURL:self.viewModel.url];
    [self.titleLabel setText:self.viewModel.title];
    [self.descriptionLabel setText:self.viewModel.descriptionText];
    [self.createdDate setText:self.viewModel.createdDate];
    UITableViewCell *cell = [[RWTCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                           reuseIdentifier:@"commentCell"];
    [CETableViewBindingHelper bindingHelperForTableView:self.commentsList
                                           sourceSignal:RACObserve(self.viewModel, comments)
                                       selectionCommand:nil
                                               viewCell:cell];
}

#pragma mark - Views Building

- (void)setupViews {
    [self addViews];
    [self setupConstraints];
    if (self.viewModel.tags) {
        [self buildTags];
    }
}

- (void)addViews {
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.imageView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.descriptionLabel];
    [self.contentView addSubview:self.tagsScrollView];
    [self.contentView addSubview:self.createdDate];
    [self.contentView addSubview:self.commentsList];
}

- (void)buildTags {
    [self.tagsScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(-50);
        make.height.equalTo(@40);
        make.left.right.equalTo(self.contentView);
    }];
    
    __block UILabel *lastTagLabel;
    @weakify(self)
    [self.viewModel.tags enumerateObjectsUsingBlock:^(NSString *string, NSUInteger index, BOOL *stop) {
        @strongify(self)
        UILabel *label = [self tagLabelWithText:string];
        [self.tagsScrollView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(self.tagsScrollView);
        }];
        if (index == 0) {
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.tagsScrollView).offset(10);
            }];
        } else {
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastTagLabel.mas_right).offset(10);
            }];
        }
        lastTagLabel = label;
    }];
    
    [self.tagsScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastTagLabel);
    }];
}

#pragma mark - Views Getters

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [UIScrollView new];
        _scrollView.layer.borderColor = [[UIColor blackColor] CGColor];
        _scrollView.layer.borderWidth = 2.0f;
    }
    
    return _scrollView;
}

- (UIView *)contentView {
    if (_contentView == nil) {
        _contentView = [UIView new];
    }
    
    return _contentView;
}

- (UIImageView *)imageView {
    if (_imageView == nil) {
        _imageView = [UIImageView new];
    }
    
    return _imageView;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        [_titleLabel setFont:[UIFont fontWithName:@"Futura-Bold" size:15]];        
    }
    
    return _titleLabel;
}

- (UILabel *)descriptionLabel {
    if (_descriptionLabel == nil) {
        _descriptionLabel = [UILabel new];
        _descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _descriptionLabel.numberOfLines = 0;
        [_descriptionLabel setFont:[UIFont fontWithName:@"Futura" size:12]];
        [_descriptionLabel setTextColor:[UIColor grayColor]];
    }
    
    return _descriptionLabel;
}

- (UILabel *)tagLabelWithText:(NSString *)text {
    UILabel *tagLabel = [UILabel new];
    tagLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.3 blue:0.3 alpha:0.70];
    tagLabel.layer.cornerRadius = 15;
    tagLabel.layer.masksToBounds = YES;
    [tagLabel setFont:[UIFont fontWithName:@"Futura" size:12]];
    [tagLabel setTextColor:[UIColor whiteColor]];
    [tagLabel setText:text];
    
    return tagLabel;
}

- (UIScrollView *)tagsScrollView {
    if (!_tagsScrollView) {
        _tagsScrollView = [UIScrollView new];
    }
    
    return _tagsScrollView;
}

- (UILabel *)createdDate {
    if (!_createdDate) {
        _createdDate = [UILabel new];
        [_createdDate setFont:[UIFont fontWithName:@"Futura" size:11]];
        [_createdDate setTextColor:[UIColor grayColor]];
    }
    
    return _createdDate;
}

- (UITableView *)commentsList {
    if (!_commentsList) {
        _commentsList = [UITableView new];
    }
    
    return _commentsList;
}

#pragma mark - Masonry Constraints

- (void)setupConstraints {
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView);
    }];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@0);
        make.width.equalTo(self.scrollView);
        make.height.equalTo(@200);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(10);
        make.left.equalTo(@10);
        make.width.equalTo(self.scrollView);
    }];
    [self.descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.equalTo(@10);
        make.width.equalTo(self.scrollView);
    }];
    [self.commentsList mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descriptionLabel.mas_bottom).offset(10);
        make.left.equalTo(@0);
        make.width.equalTo(self.scrollView);
        make.height.equalTo(@200);
    }];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.commentsList.mas_bottom);
    }];
    [self.createdDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(50);
        make.right.equalTo(self.contentView).offset(-10);
        make.height.equalTo(@40);
    }];
}

@end
