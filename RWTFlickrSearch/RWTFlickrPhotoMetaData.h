//
//  RWTFlickrPhotoMetaData.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RWTFlickrPhotoMetaData : NSObject

- (instancetype) initWithFavorites:(NSInteger)favorites
                         comments:(NSInteger)comments;

@property (nonatomic) NSInteger favorites;
@property (nonatomic) NSInteger comments;

@end
