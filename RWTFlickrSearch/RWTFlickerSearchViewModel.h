//
//  RWTFlickerSearchViewModel.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "RWTViewModelServices.h"
#import "RWTViewModelServicesImpl.h"

@interface RWTFlickerSearchViewModel : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) RACCommand *executeSearch;
@property (strong, nonatomic) id<RWTViewModelServices> viewModelServices;

- (instancetype)initWithServices:(id<RWTViewModelServices>)services;

@end
