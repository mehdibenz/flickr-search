//
//  RWTFlickrPhotoDetails.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickrPhotoDetails.h"

@implementation RWTFlickrPhotoDetails

- (instancetype)initWithDescription:(NSString *)description
                            created:(NSDate *)created
                           comments:(NSArray *)comments
                               tags:(NSArray *)tags {
    self = [super init];
    if (self) {
        _descriptionText = description;
        _created = created;
        _comments = comments;
        _tags = tags;
    }
    
    return self;
}

@end
