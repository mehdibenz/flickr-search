//
//  RWTFlickrPhotoMetaData.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickrPhotoMetaData.h"

@implementation RWTFlickrPhotoMetaData

- (instancetype)initWithFavorites:(NSInteger)favorites
                         comments:(NSInteger)comments {
    self = [super init];
    if (self) {
        _favorites = favorites;
        _comments = comments;
    }
    
    return self;
}

@end
