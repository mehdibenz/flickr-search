//
//  RWTDetailsViewModel.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTDetailsViewModel.h"
#import "RWTFlickrPhoto.h"

@interface RWTDetailsViewModel ()

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation RWTDetailsViewModel

- (instancetype)initWithPhoto:(RWTFlickrPhoto *)photo
                 photoDetails:(RWTFlickrPhotoDetails *)photoDetails
                     services:(id<RWTViewModelServices>)services {
    self = [super init];
    if (self) {
        _title = photo.title;
        _url = photo.url;
        _descriptionText = photoDetails.descriptionText;
        _tags = photoDetails.tags;
        _comments = photoDetails.comments;
        _createdDate = [self.dateFormatter stringFromDate:photoDetails.created];
    }
    
    return self;
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    }
    
    return _dateFormatter;
}
@end
