//
//  RWTSearchResultViewModel.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 01/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWTFlickrSearchResults.h"
#import "RWTViewModelServices.h"
#import "RWTSearchResultItemViewModel.h"

@interface RWTSearchResultViewModel : NSObject

- (instancetype)initWithResults:(RWTFlickrSearchResults *)results
                       services:(id<RWTViewModelServices>)services;

@property (nonatomic, strong) RACCommand * showDetailsCommand;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray<RWTSearchResultItemViewModel *> *searchResults;
@property (nonatomic, strong) id<RWTViewModelServices> services;

@end
