//
//  RWTSearchResultItemViewModel.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWTFlickrPhoto.h"
#import "RWTViewModelServices.h"

@interface RWTSearchResultItemViewModel : NSObject

- (instancetype)initWithPhoto:(RWTFlickrPhoto *)photo
                     services:(id<RWTViewModelServices>)services;

@property (nonatomic) BOOL isVisible;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSNumber *comments;
@property (nonatomic, strong) NSNumber *favorites;
@property (strong, nonatomic) RWTFlickrPhoto *photo;

@end
