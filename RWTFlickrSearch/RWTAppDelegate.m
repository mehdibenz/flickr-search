//
//  RWTAppDelegate.m
//  RWTFlickrSearch
//
//  Created by Colin Eberhardt on 20/05/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

#import "RWTAppDelegate.h"
#import "RWTFlickrSearchViewController.h"
#import "RWTFlickerSearchViewModel.h"
#import "RWTViewModelServicesImpl.h"
#import "RWTDetailsViewModel.h"
#import "RWTDetailsViewController.h"

@interface RWTAppDelegate ()

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) RWTViewModelServicesImpl *viewModelServices;

@end

@implementation RWTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // create a navigation controller and perform some simple styling
    self.navigationController = [UINavigationController new];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.0
                                                                           green:0.3
                                                                            blue:0.3
                                                                           alpha:0.70];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    // create and navigate to a view controller
    UIViewController *viewController = [self createInitialViewController];
    [self.navigationController pushViewController:viewController animated:NO];
    
    // show the navigation controller
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (UIViewController *)createInitialViewController {
    self.viewModelServices = [[RWTViewModelServicesImpl alloc]
                              initWithNavigationController:self.navigationController];
    RWTFlickerSearchViewModel *viewModel = [[RWTFlickerSearchViewModel alloc]
                                            initWithServices:self.viewModelServices];
    
    return [[RWTFlickrSearchViewController alloc] initWithViewModel:viewModel];
}

@end
