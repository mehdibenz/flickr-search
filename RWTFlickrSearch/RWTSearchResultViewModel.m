//
//  RWTSearchResultViewModel.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 01/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTSearchResultViewModel.h"
#import <LinqToObjectiveC/NSArray+LinqExtensions.h>
#import "RWTDetailsViewModel.h"
#import "RWTFlickrPhotoMetaData.h"

@implementation RWTSearchResultViewModel

- (instancetype)initWithResults:(RWTFlickrSearchResults *)results
                       services:(id<RWTViewModelServices>)services {
    self = [super init];
    if (self) {
        _title = results.searchString;
        _searchResults =  [results.photos linq_select:^id(RWTFlickrPhoto *photo) {
            return [[RWTSearchResultItemViewModel alloc] initWithPhoto:photo
                                                              services:services];
        }];
        _services = services;
    }
    
    return self;
}

- (RACCommand *)showDetailsCommand {
    return [[RACCommand alloc] initWithSignalBlock:^RACSignal *(RWTSearchResultItemViewModel *input) {
        return [[[self.services getFlickrSearchService] flickrImageDetails:input.photo.identifier]
                doNext:^(RWTFlickrPhotoDetails *photoDetails) {
                    RWTDetailsViewModel *viewModel = [[RWTDetailsViewModel alloc] initWithPhoto:input.photo
                                                                                   photoDetails:photoDetails
                                                                                       services:self.services];
                    [self.services pushViewModel:viewModel];
                }];
    }];
}

@end
