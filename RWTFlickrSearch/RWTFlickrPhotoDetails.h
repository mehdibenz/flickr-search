//
//  RWTFlickrPhotoDetails.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWTFlickrPhoto.h"
#import "RWTFlickrPhotoMetaData.h"

@interface RWTFlickrPhotoDetails : NSObject

- (instancetype)initWithDescription:(NSString *)description
                            created:(NSDate *)created
                           comments:(NSArray *)comments
                               tags:(NSArray *)tags;

@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSArray *comments;
@property (nonatomic, strong) NSArray *tags;

@end
