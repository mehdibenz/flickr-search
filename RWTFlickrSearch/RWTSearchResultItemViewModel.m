//
//  RWTSearchResultItemViewModel.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTSearchResultItemViewModel.h"
#import "ReactiveCocoa/RACEXTScope.h"
#import "RWTFlickrPhotoMetaData.h"

@interface RWTSearchResultItemViewModel ()

@property (weak, nonatomic) id<RWTViewModelServices> services;

@end

@implementation RWTSearchResultItemViewModel

- (instancetype)initWithPhoto:(RWTFlickrPhoto *)photo
                     services:(id<RWTViewModelServices>)services {
    self = [super init];
    if (self) {
        _photo = photo;
        _services = services;
        _title = photo.title;
        _url = photo.url;
        [self initialize];
    }
    
    return self;
}

- (void)initialize {
    RACSignal *fetchMetadata = [RACObserve(self, isVisible)
                                filter:^BOOL(NSNumber *visible) {
                                    return [visible boolValue];
                                }];
    
    @weakify(self)
    [fetchMetadata subscribeNext:^(id x) {
        @strongify(self)
        [[[self.services getFlickrSearchService] flickrImageMetadata:self.photo.identifier]
         subscribeNext:^(RWTFlickrPhotoMetaData *x) {
             self.favorites = @(x.favorites);
             self.comments = @(x.comments);
         }];
    }];
}

@end
