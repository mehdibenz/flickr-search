//
//  RWTFlickrPhoto.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickrPhoto.h"

@implementation RWTFlickrPhoto

- (NSString *)description {
    return [NSString stringWithFormat:@"Title:%@, Identifier:%@, Url:%@", self.title, self.identifier, self.url];
}

@end
