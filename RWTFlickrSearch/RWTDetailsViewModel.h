//
//  RWTDetailsViewModel.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWTFlickrPhotoDetails.h"
#import "RWTViewModelServices.h"

@interface RWTDetailsViewModel : NSObject

- (instancetype)initWithPhoto:(RWTFlickrPhoto *)photo
                 photoDetails:(RWTFlickrPhotoDetails *)photoDetails
                     services:(id<RWTViewModelServices>)services;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *createdDate;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) NSArray *comments;
@property (nonatomic, strong) id<RWTViewModelServices> services;

@end
