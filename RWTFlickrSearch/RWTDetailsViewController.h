//
//  RWTDetailsViewController.h
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 02/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTDetailsViewModel.h"

@interface RWTDetailsViewController : UIViewController

- (instancetype)initWithViewModel:(RWTDetailsViewModel *)viewModel;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UITableView *commentsList;
@property (nonatomic, strong) UILabel *createdDate;
@property (nonatomic, strong) UIScrollView *tagsScrollView;

@end
