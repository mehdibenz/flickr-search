//
//  RWTFlickerSearchViewModel.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickerSearchViewModel.h"
#import "RWTSearchResultViewModel.h"

@implementation RWTFlickerSearchViewModel

- (instancetype)initWithServices:(id<RWTViewModelServices>)services {
    self = [super init];
    if (self) {
        [self initialize];
        self.viewModelServices = services;
    }
    return self;
}

- (void)initialize {
    RACSignal *signal = [RACObserve(self, searchText) map:^id(id value) {
        return @([value length] > 3);
    }];
    signal = [signal distinctUntilChanged];
    [signal subscribeNext:^(id x) {
        NSLog(@"search text validity = %@", x);
    }];
    
    RACCommand *command = [[RACCommand alloc] initWithEnabled:signal
                                                  signalBlock:^RACSignal *(id input) {
                                                      return [self executeSearchSignal];
                                                  }];
    self.executeSearch = command;
}

- (RACSignal *)executeSearchSignal {
    return [[[[self.viewModelServices getFlickrSearchService]
              flickrSearchSignal:self.searchText]
             doNext:^(id result) {
                 RWTSearchResultViewModel *searchResultViewModel = [[RWTSearchResultViewModel alloc] initWithResults:result
                                                                                                            services:self.viewModelServices];
                 [self.viewModelServices pushViewModel:searchResultViewModel];
             }]
            logAll];
}

@end
