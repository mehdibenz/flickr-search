//
//  RWTViewModelServicesImpl.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTViewModelServicesImpl.h"
#import "RWTFlickrSearch.h"
#import "RWTFlickrSearchImpl.h"
#import "RWTSearchResultViewModel.h"
#import "RWTSearchResultsViewController.h"
#import "RWTDetailsViewController.h"
#import "RWTDetailsViewModel.h"

@interface RWTViewModelServicesImpl ()

@property (nonatomic, strong) RWTFlickrSearchImpl *flickerSearchService;
@property (nonatomic, strong) UINavigationController *navigationController;

@end

@implementation RWTViewModelServicesImpl

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController {
    self = [super init];
    if (self) {
        _flickerSearchService = [RWTFlickrSearchImpl new];
        _navigationController = navigationController;
    }
    
    return self;
}

- (id<RWTFlickrSearch>)getFlickrSearchService {
    return self.flickerSearchService;
}

- (void)pushViewModel:(id)viewModel {
    id viewController;
    
    if ([viewModel isKindOfClass:RWTSearchResultViewModel.class]) {
        viewController = [[RWTSearchResultsViewController alloc] initWithViewModel:viewModel];
    } else if ([viewModel isKindOfClass:RWTDetailsViewModel.class]) {
        viewController = [[RWTDetailsViewController alloc] initWithViewModel:viewModel];
    } else {
        NSLog(@"an unknown ViewModel was pushed!");
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
