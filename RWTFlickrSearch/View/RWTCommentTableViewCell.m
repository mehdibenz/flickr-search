//
//  RWTCommentTableViewCell.m
//  RWTFlickrSearch
//
//  Created by EL MAHDI BENZEKRI on 07/03/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTCommentTableViewCell.h"

@implementation RWTCommentTableViewCell

- (void)bindViewModel:(id)viewModel {
    [self.textLabel setText:viewModel];
    [self.textLabel setTextColor:[UIColor blueColor]];
    [self.textLabel setFont:[UIFont fontWithName:@"Futura" size:11]];
}

@end
