//
//  Created by Colin Eberhardt on 23/04/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

#import "RWTSearchResultsViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "RWTSearchResultsTableViewCell.h"
#import "CETableViewBindingHelper.h"

@interface RWTSearchResultsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *searchResultsTable;
@property (nonatomic, strong) RWTSearchResultViewModel *viewModel;

@end

@implementation RWTSearchResultsViewController

- (instancetype)initWithViewModel:(RWTSearchResultViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self bindViewModel];
}

- (void)bindViewModel {
    self.title = self.viewModel.title;
    UINib *cellNib = [UINib nibWithNibName:@"RWTSearchResultsTableViewCell" bundle:nil];
    [CETableViewBindingHelper bindingHelperForTableView:self.searchResultsTable
                                           sourceSignal:RACObserve(self.viewModel, searchResults)
                                       selectionCommand:self.viewModel.showDetailsCommand
                                           templateCell:cellNib];
}

@end
