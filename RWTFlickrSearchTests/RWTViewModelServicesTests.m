//
//  RWTFlickrSearchTests.m
//  RWTFlickrSearchTests
//
//  Created by Colin Eberhardt on 20/05/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RWTViewModelServices.h"
#import "RWTViewModelServicesImpl.h"
#import "RWTSearchResultViewModel.h"
#import "RWTSearchResultsViewController.h"
#import "RWTDetailsViewModel.h"
#import "RWTDetailsViewController.h"

@interface RWTFlickrSearchTests : XCTestCase

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) id<RWTViewModelServices> modelServices;

@end

@implementation RWTFlickrSearchTests

- (void)setUp {
    [super setUp];
    self.navigationController = [UINavigationController new];
    self.modelServices = [[RWTViewModelServicesImpl alloc] initWithNavigationController:self.navigationController];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testPushNotSupportedViewModel {
    NSObject *viewModel = [NSObject new];
    
    [self.modelServices pushViewModel:viewModel];
    
    XCTAssertEqual([[self.navigationController viewControllers] count], 0);
}

- (void)testPushSearchResultViewModel {
    RWTSearchResultViewModel *viewModel = [RWTSearchResultViewModel new];
    
    [self.modelServices pushViewModel:viewModel];
    
    XCTAssertEqual([[self.navigationController viewControllers] count], 1);
    XCTAssertTrue([[self.navigationController topViewController] isKindOfClass:[RWTSearchResultsViewController class]]);
}

- (void)testPushDetailsViewModel {
    RWTDetailsViewModel *viewModel = [RWTDetailsViewModel new];
    
    [self.modelServices pushViewModel:viewModel];
    
    XCTAssertEqual([[self.navigationController viewControllers] count], 1);
    XCTAssertTrue([[self.navigationController topViewController] isKindOfClass:[RWTDetailsViewController class]]);
}

@end
